//วิธีการทำ String interpolation หรือ การแก้ไขให้เป็น String
/*
class myObj{
  @override
  String toString(){
    return 'My String';
  }
}
void main(){ 
  var i = myObj(); //การดึงค่าใน myObj มาเก็บไว้ใน i
  print('Hello World! i = ${i.toString().toUpperCase()}');
}
*/
//***************************************************************************************************** */
//การกำหนดค่าให้เป็น null
/*
void main() {
  int? i;
  print(i);
}
*/
//***************************************************************************************************** */
//การกำหนดค่าไม่ให้เป็น null (ใช้ var เป็นตัวกำหนดว่าตัวแปรนั้นจะเป็นตัวแปรชนิดใดๆ)
/*
void main() {
  var i = 1;
  var b = 'My part'
  print(i);
}
*/
//***************************************************************************************************** */
// null-aware operation : เชคว่าตัวแปรนั้นเป็นค่า null ไหม
/*
void main() {
  int? i;
  if (i == null) {
    i = 3;
  } // หรือ i ??= 3; ก็ได้
  print(i);

  i ??= 5; //เชคว่าต่า i เป็น nullไหม ถ้าไม่เป็น null จะแสดงค่าของตัวแปรนั้น
  print(i);
}
*/
//หรือ
/*
void main(){
  int? a;
// กล่าวว่า b มีค่าเท่ากับ ฝั่งขวาทั้งหมดไหม
  var b = a ?? 3; // ถ้าค่าที่อยู่ด้านหน้า ?? เป็น null จะนำค่าที่อยู่หลัง ?? ใส่เข้าไป ดังนั้น a =3 และ b = a
  print(b);
}
*/
//หรือ
/*
void main(){
  int? a;
  a = 5;
// กล่าวว่า b มีค่าเท่ากับ ฝั่งขวาทั้งหมดไหม
  var b = a ?? 3; // ถ้าค่าที่อยู่ด้านหน้า ?? ไม่เป็น null จะนำค่าของตัวแปรนั้น คืนกลับไปแทน
  print(b);
}
*/
//***************************************************************************************************** */
//Conditional property access (คือการเข้าถึงตัวแปรนั้นแบบมีเงื่อนไข)
/*
class MyObj{
  int a = 3;
}

void main(){
  MyObj? o; // คือ MyObj ที่เป็น null ของ o
  // กล่าวคือ b มีโอกาสเป็น int ที่ไม่ใช่ null คือ ถ้า o เป็น null จะ return null กลับมาให้ แต่ถ้าไม่เป็น null จะ return ค่านั้นกลับมาให้
  var b = o?.a; // สาเหตุที่ o ต้องใส่ o? ด้วยเนื่องจาก o เป็น null - aware 
  print(b);
}
*/
//หรือ
/*
class MyObj{
  int a = 3;
}

void main(){
  MyObj? o; 

  var b = o?.a??5; //คือ ถ้า ฝั่งซ้าย เป็น null ให้ return ค่า 5 ออกไปเลย 
  print(b);// ได้ 5 เนื่องจาก Object เรายังไม่ได้ Defind ขึ้นมา
}
*/
//หรือ
/*
class MyObj{
  int a = 3;
}

void main(){
  MyObj? o; 
  o = MyObj(); // ทำการ Defind Object

  var b = o?.a??5; 
  print(b); //ได้ 3 เนื่องจาก a ใน class MyObj มีค่าเท่ากับ 3
}
*/
//***************************************************************************************************** */
// Collection literals ( การกำหนดค่าของ set )
/*
void main() {
  var l = [1, 2, 3]; // list ของ int
  var e = <int> [1, 2, 3]; // list ที่กำหนดให้มีค่าเป็น int
  var k = [1, 2, 3, 'aaa']; // list ของ object อะไรก็ได้ 
  var s = {1, 2, 3};
  var m = {1: 10, 2: 20, 3: 30};
}
*/
//***************************************************************************************************** */
//Arrow syntax
/*
void main() {
  var l = [1, 2, 3];

  l.forEach((element) {//เป็น function ในการวนลูปเพื่อนดึงค่าในลูปมาทีละตัวจนหมด
    print(element);
  });//เป็น Anonymous function หนึ่ง
}
*/
//หรือ
/*
void main() {
  var l = [1, 2, 3];
  // forEach ไม่ต้อง return เนื่องจากเป็น void function
  l.forEach((element) => print(element));
}//เหมือนกับตัวอย่างบนทุกประการ
*/
//***************************************************************************************************** */
//Cascades
// Ex.1
/*
class MyObj{
  int i = 3;
  int add(int n){
    i += n;
    return i;
  }
}

void main(){
  var o = MyObj();

  var a = o.add(2);
  var b = o..add(2); //run คำสั่งให้แต่ return เป็น Object อยู่ดี

  print(a);
  print(b);
}
*/
//Ex.2
/*
class MyObj{
  int i = 3;
  int add(int n){
    i += n;
    return i;
  }
}

void main(){
  var o = MyObj();

  var a = o.add(2);
  var b = o..add(2)..add(3)..add(1);  //การทำซำ้ๆ แล้วโยนไปเป็น Object

  print(a);
  print(b);
}
*/
//***************************************************************************************************** */
//Getter and Setter
/*
class MyObj{
  int i = 3;
  int _j = 4; // ใส่ final int _j = 4; เมื่อไม่มีการเปลี่ยนค่า

  int get jj => _j * _j;

  int add(int n){

    i += n;
    return i;
  }
}

void main(){
  var o = MyObj();

  var a = o.add(2);
  var b = o..add(2)..add(3)..add(1);  //การทำซำ้ๆ แล้วโยนไปเป็น Object
  print(o.jj);
  print(a);
  print(b);
}
*/
//***************************************************************************************************** */
//Optional positional parameters
//แบบยังไม่เป็น Optional
/*
void main(){
var x = mysum(1, 2, 3);
print(x);
}

int mysum(int a, int b, int c){
  return a + b + c;
}
*/
//แบบเป็น Optional
/*
void main(){
var x = mysum(1); //กรณีที่ใส่ค่ามาตัวเดียว
print(x);
}

int mysum(int a, [int? b, int? c]){ //b กับ c ไม่มีค่า เลยกำหนดให้เป็น null แล้วไปใส่ค่าข้างล่างแทน หรือใส่ค่า defult ให้มัน (int a, [int b=8, int? c])
  return a + (b=2) + (c=3); // หรือ return a + (b??2) + (c??3);
}
*/
//***************************************************************************************************** */
//Optional named parameters { defult ของ name คือ optional คือ กำหนดให้เป็น null หรือค่า defult}
/*
void main(){
//var x = mysum(); //เราจะไม่ใส่ค่าอะไรเลยก็ได้ หรือ
// ใส่ค่าให้ตัวไหนก็ได้
var m = mysum(a: 4, c: 8);

//print(x);
print(m);
}

int mysum({int? a, int? b, int? c}){
  return (a??1) + (b??2) + (c??2);
}
*/
//หรือ
/*
void main() {
  var m = mysum(c: 8);
  print(m);
}

int mysum({int? a, int? b, required int c}) {// require คือต้องใส่ค่านั้นเสมอ
  return (a ?? 1) + (b ?? 2) + c;
}
*/
//Finish Learn No.1